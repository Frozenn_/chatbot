import chatGPT from './chatgpt';
import Tchat from './discussion';
import InseeAnswer from './insee';
import getCodePostal from './code_postal';
// import fs from 'fs';
// import path from 'path';

const bots = [{
  id: '1',
  name: 'Zlatan',
  avatar: 'https://img.20mn.fr/BgiLOjCcS8-xB032tV1_3Ck/1200x768_sweden-s-forward-zlatan-ibrahimovic-attends-press-conference-following-a-training-session-of-sweden-s-national-football-team-in-solna-on-march-21-2023-ahead-to-the-euro-qualifier-of-sweden-vs-belgium-to-be-played-on-march-24-2023-photo-by-jonathan-nackstrand-afp',
  totalMsg: 0,
  actions: [{
    name: 'hello',
    keywords: ['hello', 'salut'],
    desc: 'Zlatan vous réponds dans sa langue natale',
    response: () => 'Hej, jag behöver inga troféer för att veta att jag är bäst.'
  }, {
    name: 'time',
    keywords: ['date', 'time', 'donne moi la date'],
    desc: 'connaitre le temps tu voulais, tel tu connaitras',
    response: () => new Date().toLocaleString('sv-SE', { timeZone: 'UTC' })
  }, {
    name: 'Code Postal',
    keywords: ['!cp', '!codes', '!code_postal'],
    desc: 'connaitre la ville d\'après une série de 5 chiffre, bizarre',
    response: async (code) => {
      const result = await getCodePostal(code);
      console.log(result); // Affiche la valeur résolue de la promesse dans la console du navigateur
      return result; // Renvoie directement la valeur résolue
    }
  }]
}, {
  id: '2',
  name: 'Superman',
  avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRnowJzsfyH-IMdC8nd9PpdyQ-aeagOZp4gQg&usqp=CAU',
  totalMsg: 0,
  actions: [{
    name: 'hello',
    keywords: ['hello', 'hi'],
    desc: 'Superman révele son identité',
    response: () => 'Hello, i\'m Clark Kent'
  }, {
    name: 'stressed out',
    keywords: ['!stress', '!stressed out'],
    desc: 'une petite chanson, ça remotive ?',
    response: () => `My name's Blurryface and I care what you think <br/>
      Wish we could turn back time<br/>
      To the good old days<br/>
      When our mama sang us to sleep<br/>
      But now we're stressed out (oh)<br/>
      Wish we could turn back time (oh)<br/>
      To the good old days (oh)<br/>
      When our mama sang us to sleep<br/>
      But now we're stressed out<br/>
      We're stressed out<br/>
    `
  }, {
    name: 'chatgpt',
    keywords: ['chatgpt', 'gpt'],
    desc: 'un programme qui parle à un programe pour savoir quoi te répondre',
    response: (txt) => chatGPT(txt)
  }]
}, {
  id: '3',
  name: 'Groot',
  avatar: 'https://cdn.shopify.com/s/files/1/0019/9881/5268/articles/Bebe_groot_4_figurines_differentes_pot_de_fleurs_ou_a_crayons_mignon_et_attachant_1024x1024.jpg?v=1534287302',
  totalMsg: 0,
  actions: [{
    name: 'hello',
    keywords: ['hello', 'bonjour'],
    desc: 'Son nom tu connaitrais, mais pas plus...',
    response: () => 'Je s\'apelle Groot.'
  }, {
    name: 'time',
    keywords: ['date', 'time', 'donne moi la date'],
    desc: 'connaitre le temps tu voulais, tel tu connaitras',
    response: () => new Date().toLocaleString('fr-FR', { timeZone: 'UTC' })
  }, {
    name: 'insee',
    keywords: ['insee'],
    desc: 'donnez-moi 9 chiffres, je trouverais l\'entreprise qui y correspond',
    response: (txt) => {
      const res = InseeAnswer(txt);
      return res;
    }
  }]
}];

const tchat = new Tchat(bots);

tchat.run();
