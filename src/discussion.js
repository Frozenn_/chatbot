import Bot from './bot';

const Tchat = class {
  constructor(bots) {
    this.el = document.querySelector('#app');
    this.bots = bots.map((bot) => new Bot(bot));
    this.messages = this.LocalStorageInitialization();
  }

  renderBot(bot) {
    const {
      name,
      avatar,
      totalMsg,
      id
    } = bot;

    return `
      <li id="bot-${id}" class="border-1 bg-secondary list-group-item d-flex justify-content-between align-items-center">
        <img width="50" class="rounded-circle" src="${avatar}" />
        <span class="h6">${name}</span>
        <span class="badge bg-primary rounded-pill">${totalMsg}</span>
      </li>
    `;
  }

  renderMessageRECU(message) {
    const {
      author,
      date,
      text,
      avatar
    } = message;

    return `
    <div class="row mb-2">
      <div class="col-6">
        <div class="card">
          <div class="card-header bg-success">
            <img height="50" class="rounded-circle" src="${avatar}"/>
            <span class="h5 ps-3">${author}</span>
          </div>
          <div class="card-body">
            <h5 class="card-title">${date}</h5>
            <p class="card-text">${text}</p>
          </div>
        </div>
      </div>
      <div class="col-6"></div>
    </div>
    `;
  }

  renderMessageENVOYER(message) {
    const {
      author, date, text, avatar
    } = message;
    return `
    <div class="row mb-2">
      <div class="col-6"></div>
      <div class="col-6">
        <div class="card">
          <div class="card-header bg-warning">
            <img width="50" class="rounded-circle" src="${avatar}" />
            <span class="h5 ps-3">${author}</span>
          </div>
          <div class="card-body">
            <h5 class="card-title">${date}</h5>
            <p class="card-text">${text}</p>
          </div>
        </div>
      </div>
    </div>`;
  }

  renderMessagesList() {
    return `
      <div class="list-messages col-9 mt-4">
        ${this.messages.map((message) => (message.type === 'bot' ? this.renderMessageRECU(message) : this.renderMessageENVOYER(message))).join('')}
      </div>
    `;
  }

  render() {
    return `
      <header class="fixed-top">
        <nav class="navbar bg-success">
          <div class="container-fluid">
            <span class="navbar-brand mb-0 h1"><strong>Bot JS Vanilla Tchat</span>
          </div>
        </nav>
      </header>
      <main class="container-fluid pt-0 mt-5 pb-5">
        <div class="row">
          <div class="col-3 mt-2 bg-dark">
            <ul class="list-group">
              ${this.bots.map((bot) => this.renderBot(bot)).join('')}
            </ul>
          </div>
          ${this.renderMessagesList()}
        </div>
        <div class="row fixed-bottom bg-light">
          <div class="col-9 align-self-center">
            <div class="input-group mt-3 mb-3">
              <input type="text" class=" form-control" placeholder="Écris ton message ici" aria-label="Recipient's username" aria-describedby="button-addon2">
              <button class="btn btn-primary" type="button" id="button-addon2">Envoyer votre message</button>
            </div>
          </div>
        </div>
      </main>
    `;
  }

  updateMsgCount() {
    const botsEl = document.querySelectorAll('.list-group li');

    botsEl.forEach((botEl, index) => {
      const badgeEl = botEl.querySelector('.badge');

      badgeEl.textContent = this.bots[index].TotalMsg;
    });
  }

  getBotsResponse(keyword) {
    const messages = [];
    if (keyword.toLowerCase() === 'help' || keyword.toLowerCase === 'aide') {
      messages.push({
        id: 0,
        author: 'Sunny',
        avatar: 'https://myrhline.com/wp-content/uploads/2019/12/travail-et-robot-image-du-film-i-robot.jpg.webp',
        text: `
        Les commandes disponibles sont: \n
         - hello(tous), usage = hello, hi, salut, bonjour, 
         - date(tous), usage= time, date,
         - chatgpt(superman), usage = chatgpt<demande> ou gpt <demande>,
         - insee(groot), usage = insee <siren>,
         - code_postal(Zlatan), usage = !CP, !codes, !code_postal,
        `,
        date: new Date().toLocaleString(),
        type: 'bot'
      });
    } else {
      this.bots.forEach((bot) => {
        const responses = bot.FindActions(keyword);
        if (responses.length) {
          const { avatar, name, id } = bot;
          responses.forEach((text) => {
            messages.push({
              id,
              author: name,
              avatar,
              text,
              date: new Date().toLocaleString(),
              type: 'bot'
            });
          });
        }
      });
    }
    return messages;
  }

  LocalStorageInitialization() {
    if (!localStorage.getItem('messages')) {
      localStorage.setItem('messages', '[]');

      return [];
    }

    return JSON.parse(localStorage.getItem('messages'));
  }

  setStorageMessages(messages) {
    this.messages = this.messages.concat(messages);
    localStorage.setItem('messages', JSON.stringify(this.messages));
  }

  setStorageMessage(message) {
    this.messages.push(message);
    localStorage.setItem('messages', JSON.stringify(this.messages));
  }

  doWhenClickEnterOrButton() {
    const elInput = document.querySelector('input');

    const elListMessage = document.querySelector('.list-messages');
    const { value } = elInput;

    const message = {
      author: 'Me',
      avatar: 'https://media.licdn.com/dms/image/C4E03AQEWwEfqQkiv0g/profile-displayphoto-shrink_200_200/0/1603958670008?e=1688601600&v=beta&t=i1GZTwOKAe6kjJD8KueulfqEP9nujywJyP3tcVq54p0',
      date: new Date().toLocaleString(),
      text: value,
      type: 'user'
    };
    const messagesBots = this.getBotsResponse(value);
    elListMessage.innerHTML += this.renderMessageENVOYER(message);
    elListMessage.innerHTML += messagesBots.map((msg) => this.renderMessageRECU(msg)).join('');
    elListMessage.scrollTo(0, elListMessage.scrollHeight);
    this.setStorageMessage(message);
    this.setStorageMessages(messagesBots);
    this.updateMsgCount();
    elInput.value = '';
  }

  onClickSendMessage() {
    document.addEventListener('keydown', (event) => {
      if (event.key === 'Enter') {
        this.doWhenClickEnterOrButton();
        event.preventDefault();
      }
    });

    const elBtn = document.querySelector('button');
    elBtn.addEventListener('click', () => {
      this.doWhenClickEnterOrButton();
    });
  }

  run() {
    this.el.innerHTML = this.render();
    this.onClickSendMessage();
    this.updateMsgCount();
  }
};

export default Tchat;
