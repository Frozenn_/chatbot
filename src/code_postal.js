// import request from 'request';
// const request = require('request');
const axios = require('axios');

const getCodePostal = (cp) => new Promise((resolve, reject) => {
  const code = cp.split(' ')[1].trim();
  const config = {
    method: 'get',
    url: `https://apicarto.ign.fr/api/codes-postaux/communes/${code}`,
    headers: {}
  };

  axios(config)
    .then((response) => {
      let txt = '';
      response.data.forEach((element) => {
        txt += `commune: ${element.nomCommune} \n`;
      });
      return resolve(txt); // Résoudre la promesse avec la valeur txt
    })
    .catch((error) => reject(error)); // Rejeter la promesse avec l'erreur
});
  // request(options, (error, response) => {
  //   if (error) throw new Error(error);
  //   let txt = '';
  //   response.body.forEach((element) => {
  //     txt += `commune: ${element.nomCommune} \n`;
  //   });
  //   return txt;
  // });
// };

export default getCodePostal;
