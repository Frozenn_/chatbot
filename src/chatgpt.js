import { Configuration, OpenAIApi } from 'openai';

const chatGPT = async (txt) => {
  const message = txt.split('gpt')[1].trim();
  const configuration = new Configuration({
    apiKey: '<VOTRE CLE API ICI>' // Idéalement, vous aurez mis votre clé api dans l'env car c'est mieux mais le modules fs de dotenv m'as dit NOPE
  });

  const openai = new OpenAIApi(configuration);

  const completion = await openai.createChatCompletion({
    model: 'gpt-3.5-turbo',
    messages: [{ role: 'user', content: message }]
  });
  return completion.data.choices[0].message;
};

export default chatGPT;
