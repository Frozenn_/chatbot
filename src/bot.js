const Bot = class {
  constructor(bot) {
    const {
      id,
      name,
      avatar,
      totalMsg,
      actions
    } = bot;

    this.id = id;
    this.name = name;
    this.avatar = avatar;
    this.totalMsg = totalMsg;
    this.actions = actions;
  }

  setCounterInLocalStorage() {
    const counter = {
      totalMsg: this.totalMsg,
      id: this.id
    };

    if (localStorage.getItem('botsCounters')) {
      const botsCounter = JSON.parse(localStorage.getItem('botsCounters'));

      const ifExist = botsCounter.find((botCounter) => botCounter.id === this.id);

      if (!ifExist) {
        botsCounter.push(counter);

        localStorage.setItem('botsCounters', JSON.stringify(botsCounter));

        return;
      }

      localStorage.setItem('botsCounters', JSON.stringify(
        botsCounter.map((botCounter) => {
          const newCounter = (botCounter.id === this.id) ? counter : botCounter;

          return newCounter;
        })
      ));

      return;
    }

    localStorage.setItem('botsCounters', JSON.stringify([counter]));
  }

  listActions() {
    const responses = [];

    this.actions.forEach((action) => {
      const { name } = action;
      responses.push(name);
    });
    return responses;
  }

  FindActions(keyword) {
    const responses = [];
    const key = keyword.split(' ').length > 1 ? keyword.split(' ')[0] : keyword;
    this.actions.forEach((action) => {
      const { keywords, response } = action;
      if (keywords.includes(key.toLowerCase())) {
        responses.push(response(keyword));

        this.totalMsg += 1;

        this.setCounterInLocalStorage();
      }
    });

    return responses;
  }
};

export default Bot;
