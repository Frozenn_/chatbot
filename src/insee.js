// import request from 'request';
// const request = require('request');
const axios = require('axios');
const qs = require('qs');
const FormData = require('form-data');

const getTokenAccessInsee = () => {
  const data = qs.stringify({
    grant_type: 'client_credentials'
  });
  const config = {
    method: 'post',
    url: 'https://api.insee.fr/token',
    headers: {
      Authorization: 'Basic ZEIzUFRkZW5zTXNZa1llXzdmMVQ0amJvTEZBYTpKR1ZOQ1hhY1ZKX3NJVTVCUzFpZkxXZzl3QzRh',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*' // Bah il est la le header  NVM
    },
    data
  };
  axios(config)
    .then((response) => response.data.accessToken)
    .catch((error) => error);
};

const InseeAnswer = (siren) => {
  const accessToken = getTokenAccessInsee();
  const data = new FormData();

  const config = {
    method: 'GET',
    url: `https://api.insee.fr/entreprises/sirene/V3/siren/${siren.split('insee')[1].trim()}`,
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'Access-Control-Allow-Origin': '*'
    },
    data
  };
  axios(config)
    .then((response) => {
      const body = response.data;
      console.log(body);
      return `
        date création = ${body.uniteLegale.dateCreationUniteLegale},
        categorie = ${body.uniteLegale.categorieEntreprise},
        nom de l'entreprise = ${body.uniteLegale.periodesUniteLegale[0].nomUniteLegale}
      `;
    })
    .catch((error) => error);
  // request(options, (error, response) => {
  //   if (error) throw new Error(error);
  //   return `
  //     date création = ${response.body.uniteLegale.dateCreationUniteLegale},
  //     categorie = ${response.body.uniteLegale.categorieEntreprise},
  //     nom de l'entreprise = ${response.body.uniteLegale.periodesUniteLegale[0].nomUniteLegale}
  //   `;
  // });
};

export default InseeAnswer;
