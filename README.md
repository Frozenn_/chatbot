## Installation

Use the package manager [npm](https://www.npmjs.com/) to install helloworld.

## CHATGPT
You have to add you key in ./src/tools.js
At the line 5 replace <VOTRE CLE API ICI> by your own ApiKey

## prerequisite

Use node v16 use :
```bash
nvm install 16
```

```bash
npm i
```

## Usage

Start the application dev with :

```bash
npm run start
```

Created the dist with :

```bash
npm run dist
```

Analyse the coding rules with :

```bash
npm run lint
```
